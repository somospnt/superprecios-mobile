angular.module('starter.controllers', [])

        .controller('DashCtrl', function($scope, Comercios) {
            $scope.busqueda = {"buscando":false};
            $scope.comercios = {};
            $scope.buscarProductos = function() {
                $scope.busqueda.buscando = true;
                $scope.comercios = {};
                Comercios.buscarProductos($scope.busqueda.keywords).then(function(comercios) {
                    $scope.comercios = comercios.data;
                    $scope.busqueda.buscando = false;
                });
            };
            
            $scope.obtenerPrecio = function (producto, origen){
                return producto[map[origen]];
            }
            
            var map = {
                COTO: "precioCoto",
                CUIDADOS:"precioCuidados",
                DISCO: "precioDisco",
                JUMBO: "precioJumbo",
                WALMART:"precioWalmart"
            };
            
        })

        .controller('ChatsCtrl', function($scope, Chats) {
            $scope.chats = Chats.all();
            $scope.remove = function(chat) {
                Chats.remove(chat);
            }
        })

        .controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
            $scope.chat = Chats.get($stateParams.chatId);
        });
